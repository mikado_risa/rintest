﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZangekiEffect : MonoBehaviour
{

    private float setActiveTime = 0;
    private float deletsTime;
    private float totalTime = 0f;
    private bool nowDraw = false;



    // Use this for initialization
    void Start()
    {
        totalTime = 0;
        nowDraw = false;
    }

    // Update is called once per frame
    void Update()
    {
        totalTime += Time.deltaTime;

        if (nowDraw == false && setActiveTime < totalTime)
        {
            nowDraw = true;
            gameObject.GetComponent<SpriteRenderer>().color = Color.white;
            this.gameObject.GetComponent<PolygonCollider2D>().enabled = true;
        }
        else if (nowDraw && totalTime > deletsTime)
        {
            Destroy(this.gameObject);
            //Debug.Log("totaltime=" + totalTime + ">deleteTime=" + deletsTime);
        }
    }

    public void SetZangeki(Vector3 position,Vector3 rotation, int count)
    {
        Vector3 offset = new Vector3(0, 0, 130);
        transform.localPosition = position;
        transform.eulerAngles = rotation + offset;

        setActiveTime = count/5 * 0.01f;
        deletsTime = 0.009f + setActiveTime;//0.5秒で消える






    }


}
