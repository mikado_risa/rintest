﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineCollection : MonoBehaviour
{

    //外部から取得する
    private GameObject arrowPool = null;

    private Line linePrefab = null;


    List<Line> touchLine = new List<Line>();

    //arrowPoolの中に入れる
    private GameObject myArrowPool = null;

    int touchListActiveCount = 0;//どこまでアクティブになっているか

    float touchLenght = 10f;//この距離離れたらスワイプになる

    const int ZangekiCount = 100;//最大一回の斬撃のポイント

    const float c_distanceLinePoint = 0.16f;//補間した際の生成するポイントの距離

    public int lineID = 0;//斬撃での複数判定をなくすため

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    //public void SetArrowPool(GameObject arrow)
    //{
    //    arrowPool = arrow;
    //}


    //originPositionがおかしい疑惑、、後で直す


    public void UpDateTouch(/*Touch touch*/Vector3 touchPosition)
    {
        Debug.Log("ラインコレクションとおっています");
        //最大数以上は作れない
        if (ZangekiCount < touchListActiveCount) return;

        Vector3 ray = Camera.main.ScreenToWorldPoint(touchPosition);

        if (touchListActiveCount == 0)
        {
            if (touchLine.Count > 0)
            {
                touchLine[0].gameObject.SetActive(true);
            }
            else//(touchLine.Count == 0)
            {

                Debug.Log("linePrefab=" + linePrefab + "  myArrowPool=" + myArrowPool);


                touchLine.Add(Instantiate(linePrefab, myArrowPool.transform));
                touchLine[0].GetComponent<SpriteRenderer>().color = Color.clear;

                //タップの場合があるから表示しない
            }

            touchListActiveCount = 1;
            touchLine[0].transform.localPosition = touchLine[0].originPosition = new Vector3(ray.x, ray.y, 0);

        }
        else
        {

            //前との距離を求めて、長くして、回転して、間の座標に設置
            //   touchLine[touchLine.Count - 1].originPosition = new Vector3(ray.x, ray.y, 0);
            int MaxInterpolationCount = 4;//前の点との間に最大４回補間する
            int addCount = 0;
            while (addCount < MaxInterpolationCount)
            {

                Vector2 length = ray - touchLine[touchListActiveCount - 1].originPosition;
                float kyori = Vector2.Distance(Vector2.zero, length);
                //   Debug.Log("距離" + kyori + ">" + c_distanceLinePoint);
                //距離を求めて一定より小さかったら生成しない
                if (kyori > c_distanceLinePoint)//近くはだめ
                {
                    if (touchLine.Count > touchListActiveCount)//activeのカウントがリストより小さければ生成せずに表示
                    {
                        touchLine[touchListActiveCount].gameObject.SetActive(true);
                        touchListActiveCount++;// = touchLine.Count;
                    }
                    else
                    {
                        //  Debug.Log("touchLine.Count > touchListActiveCount=" + touchLine.Count + ">" + touchListActiveCount);
                        touchLine.Add(Instantiate(linePrefab, myArrowPool.transform).GetComponent<Line>());
                        touchListActiveCount++;// = touchLine.Count;
                    }
                    length.Normalize();//ベクトルは同じ方向は維持したままで長さが 1.0 
                    length *= c_distanceLinePoint;   // 移動予定地点の間隔
                                                     //   movePoints.Add(target + vec);
                                                     //  touchLine[touchLine.Count - 1].transform.localPosition= touchLine[touchLine.Count - 1].originPosition = touchLine[touchLine.Count - 2].originPosition + new Vector3(length.x,length.y,0);
                    touchLine[touchListActiveCount - 1].transform.localPosition = touchLine[touchListActiveCount - 1].originPosition = touchLine[touchListActiveCount - 2].originPosition + new Vector3(length.x, length.y, 0);



                    Quaternion rotate = new Quaternion();
                    Vector2 target;
                    target = touchLine[touchListActiveCount - 2].originPosition;

                    rotate.eulerAngles = new Vector3(0, 0, Mathf.Atan2(touchLine[touchListActiveCount - 1].originPosition.y - target.y, touchLine[touchListActiveCount - 1].originPosition.x - target.x) * Mathf.Rad2Deg);
                    touchLine[touchListActiveCount - 1].transform.rotation = rotate;
                    //Debug.Log("rota"+rotate);
                    //  Debug.Log("touchLine[" + touchLine.Count + "]=" + touchLine[touchLine.Count - 1].transform.localPosition+"  addCount="+addCount);
                }
                else
                    break;



                addCount++;
            }
        }
    }


    public void AttackActive(GameObject zyuuPrefab, ZangekiEffect zangekiPrefab)
    {
        if (touchListActiveCount > 1)//スライドじゃなければタップになる
        {
            for (int i = 0; i < touchListActiveCount; i++)
            {
                //      斬撃を発動させる
                if (i % 5 == 1)//0だと最初の角度がおかしいため
                    touchLine[i].Zangeki(zangekiPrefab, touchLine[i].transform.localEulerAngles, i);
                touchLine[i].gameObject.SetActive(false);
            }

        }
        else
        {
            GameObject go = Instantiate(zyuuPrefab);
            go.transform.localPosition = touchLine[0].transform.position;
            touchListActiveCount = 0;
        }
        touchListActiveCount = 0;
    }

    //タッチ数が変更した時にどこが放されたか調べるために、ラストの位置取得
    public Vector3 GetLastActivePosition()
    {
        return touchLine[touchListActiveCount - 1].transform.position;
    }

    //TODO:test後削除
    public Vector3 GetFirstPosition()
    {
        return touchLine[0].transform.position;
    }



    public void SetLineCollection(GameObject arrowPool, Line linePrefab, GameObject arrowPrefab)//ただの空のプレファブを渡している、、、（わからん）
    {
        this.linePrefab = linePrefab;
        myArrowPool = Instantiate(arrowPrefab, arrowPool.transform);

    }

    public void MoveLines(Vector3 moveVector)
    {
        //アクティブなやつのみ移動
        for (int i = 0; i < touchListActiveCount; i++)
        {
            touchLine[i].MoveLine(moveVector);
        }
    }

}
