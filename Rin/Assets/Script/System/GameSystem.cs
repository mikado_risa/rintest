﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class GameSystem : MonoBehaviour
{
    public static GameSystem I;

    [SerializeField]
    Player player = null;

    [SerializeField]
    UnityEngine.UI.Text debugText = null;

    [SerializeField]
    InputManager inputManager = null;

    [SerializeField]
    UnityEngine.UI.Text scoreText = null;

    public bool PCDebugMode = true;


    public const int SlashPower = 30;
    public const int ShotPower = 50;


    private int score = 0;
    public void AddScore(int point)
    {
        score += point;
        scoreText.text = "Score:" + score;
    }

    // Use this for initialization
    void Start()
    {
        I = this;
    }

    void Update()
    {
        if (test == true)
        {
            if (testList2[2].transform.position != testList2[1].transform.position)
            {
                float zyuryoku = 2.5f;
                float moveY = 6.5f;
                Vector3 add = testList2[1].transform.position - testList2[2].transform.position;
                if (syosoku1 == Vector3.zero)
                {
                    kakudo1 = Mathf.Atan2(
                        testList2[1].transform.position.y
                        - testList2[2].transform.position.y
                        ,
                        testList2[1].transform.position.x
                        - testList2[2].transform.position.x
                        );

                  //  Debug.Log("角度=" + kakudo1);
                    syosoku1 = add.normalized * moveY;
              //      Debug.Log("初速度"+syosoku1.ToString("F9"));

                    kakudo2 = Mathf.Atan2(
                                       testList2[0].transform.position.y
                                       - testList2[2].transform.position.y
                                       ,
                                       testList2[0].transform.position.x
                                       - testList2[2].transform.position.x
                                       );

                 //   Debug.Log("角度=" + kakudo2);

                    add = testList2[0].transform.position - testList2[2].transform.position;

                    syosoku2 = add.normalized * moveY;
                   // Debug.Log("初速度2" + syosoku2.ToString("F9"));

                }
                testList2[1].transform.position +=
                    new Vector3(
                        syosoku1.x * Mathf.Cos((float)kakudo1) * (time1 + Time.deltaTime)
                        - syosoku1.x * Mathf.Cos((float)kakudo1 ) * (time1)
                    ,
                    syosoku1.y * Mathf.Sin((float)kakudo1 ) * (time1 + Time.deltaTime) - 0.5f * zyuryoku * Mathf.Pow(time1 + Time.deltaTime, 2)
                    -
                    syosoku1.y * Mathf.Sin((float)kakudo1 ) * time1 - 0.5f * zyuryoku * Mathf.Pow(time1, 2)
                    ,
                    0);


                testList2[0].transform.position +=
                    new Vector3(
                        syosoku2.x * Mathf.Cos((float)kakudo2) * (time1 + Time.deltaTime)
                        - syosoku2.x * Mathf.Cos((float)kakudo2) * (time1)
                    ,
                    syosoku2.y * Mathf.Sin((float)kakudo2) * (time1 + Time.deltaTime) - 0.5f * zyuryoku * Mathf.Pow(time1 + Time.deltaTime, 2)
                    -
                    syosoku2.y * Mathf.Sin((float)kakudo2) * time1 - 0.5f * zyuryoku * Mathf.Pow(time1, 2)
                    ,
                    0);

                Debug.Log("位置"+testList2[1].transform.position.ToString("F9"));
                    time1 += Time.deltaTime;
            }
        }
    }
    float time1 = 0;
    float time2 = 0;
    Vector3 syosoku1 = Vector3.zero;
    Vector3 syosoku2 = Vector3.zero;
    double kakudo1 = 0;
    double kakudo2 = 0;
    public void LoadScene()
    {
        Application.LoadLevel("prototype");
    }

    //いったん主人公が消えたら終了
    public void GameOver()
    {
        debugText.text = "GameOver";
        player.GetComponent<SpriteRenderer>().color = Color.clear;
    }

    public Vector3 GetPlayerPosition()
    {
        return player.ReturnPosition();
    }
    public Player GetPlayer()
    {
        return player;
    }

    public void MoveLine(Vector3 moveVector)
    {
        inputManager.MoveLineCollection(moveVector);
    }

    public void EndGame()
    {
        Application.Quit();
    }

    bool test = false;
    List<GameObject> testList2 = new List<GameObject>();
    public void CutTest()
    {
        List<SpriteSlicer2DSliceInfo> testList = new List<SpriteSlicer2DSliceInfo>();
        SpriteSlicer2D.SliceAllSprites(new Vector3(10, 1.34f, 0), new Vector3(-10, 1.34f, 0), false, ref testList);
        test = true;
        for (int i = 0; i < testList[0].ChildObjects.Count; i++)
        {
            testList2.Add(testList[0].ChildObjects[i]);

        }
        testList2.Add(testList[0].SlicedObject);


    }



}
