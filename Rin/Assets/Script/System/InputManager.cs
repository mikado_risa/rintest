﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    [SerializeField]
    Line linePrefab;

    [SerializeField]
    UnityEngine.UI.Text text = null;

    [SerializeField]
    UnityEngine.UI.Text debugText = null;

    [SerializeField]
    GameObject zyuuPrefab = null;

    [SerializeField]
    ZangekiEffect zangekiPrefab = null;

    [SerializeField]
    GameObject SliceTestObject = null;

    [SerializeField]
    GameObject arrowPoolPrefab = null;

    [SerializeField]
    UnityEngine.UI.Text zangekiTest = null;
    List<UnityEngine.UI.Text> textList = new List<UnityEngine.UI.Text>();

    private GameObject arrowPool = null;

    Dictionary<int, LineCollection> touchDic = new Dictionary<int, LineCollection>();
    List<LineCollection> stockList = new List<LineCollection>();


    void Start()
    {
        arrowPool = GameObject.Find("ArrowPool");
    }

    
    void Update()
    {
        for (int i = 0; i < Input.touchCount; i++)
        {
            Touch touch = Input.touches[i];
            int id = touch.fingerId;

            //追加
            if (Input.touches[i].phase == TouchPhase.Began)
            {
                LineCollection line;
                if (stockList.Count == 0)
                {
                    line = new LineCollection();
                    line.SetLineCollection(arrowPool, linePrefab, arrowPoolPrefab);
                }
                else
                {
                    line = stockList[0];
                    stockList.Remove(stockList[0]);
                }
                touchDic.Add(id, line);
            }

            //削除
            if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
            {
                touchDic[id].AttackActive(zyuuPrefab, zangekiPrefab);
                stockList.Add(touchDic[id]);
                touchDic.Remove(id);
            }
            else if(touch.phase==TouchPhase.Moved|| touch.phase == TouchPhase.Began)
            {
                touchDic[id].UpDateTouch(touch.position);
            }
        }

        //パソコン用
        //TODO:パソコン時斬撃で切るようにする（とデバックで便利かな？
        if (Debug.isDebugBuild && GameSystem.I.PCDebugMode && Input.GetMouseButtonDown(0))
        {
            GameObject g = Instantiate(zyuuPrefab);
            g.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            g.transform.position = new Vector3(g.transform.position.x, g.transform.position.y, 0);
        }
    }


    public void MoveLineCollection(Vector3 moveVector)
    {
        foreach (KeyValuePair<int,LineCollection> pair in touchDic)
        {
            pair.Value.MoveLines(moveVector);
        }
    }
}