﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public int playerHP;
    private const int MaxHp = 100;
    private const float moveSpeed = 0.035f;

    [SerializeField]
    UnityEngine.UI.Slider HpSlider = null;   

    public bool isMove = false;


    public bool hasDamage = false;

    SpriteRenderer spriteRenderer = null;

    float damageSaveTime = 0f;
    float totalTime = 0;

    // Use this for initialization
    void Start()
    {
        HpSlider.maxValue = MaxHp;
        HpSlider.value = MaxHp;
        playerHP = MaxHp;
        spriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();//たぶんカラーじゃできない、いちおやってみる
    }

    // Update is called once per frame
    void Update()
    {
        if (isMove == true)
            MovePlayer();
    }

    private void MovePlayer()
    {

        this.gameObject.transform.transform.position += new Vector3(moveSpeed, 0, 0);
        GameSystem.I.MoveLine(new Vector3(moveSpeed, 0, 0));

        if (hasDamage)
            DamageEffect();
    }

    void DamageEffect()
    {
        float damageTime = 0.5f;//無敵状態時間
        float changeSpriteTime = 0.1f;//スプライト切り替える時間

        damageSaveTime += Time.deltaTime;
        totalTime += Time.deltaTime;
        if (damageSaveTime > changeSpriteTime)
        {
            if (spriteRenderer.color == Color.white)
                spriteRenderer.color = Color.red;
            else
                spriteRenderer.color = Color.white;
            damageSaveTime = 0;

        }

        if (totalTime > damageTime)
        {
            hasDamage = false;
            damageSaveTime = 0f;
            totalTime = 0;
            spriteRenderer.color = Color.white;

        }


    }

    public Vector3 ReturnPosition()
    {
        return this.gameObject.transform.transform.localPosition;
    }

    public float RetuenLocalPositionX()
    {
        return this.gameObject.transform.transform.localPosition.x;
    }

    public void Damage(int damagePoint)
    {
        hasDamage = true;
        playerHP -= damagePoint;
        if (playerHP <= 0)
        {
            playerHP = 0;
            GameSystem.I.GameOver();
        }
        HpSlider.value = playerHP;
    }

    public void MoveButton()
    {
        if (isMove == true)
            isMove = false;
        else
            isMove = true;
    }
}
