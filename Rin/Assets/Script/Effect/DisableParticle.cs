﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableParticle : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(ParticleWorking());
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator ParticleWorking()
    {
        var particle = GetComponent<ParticleSystem>();

        yield return new WaitWhile(() => particle.IsAlive(true));
        Destroy(this.gameObject);

    }
}
