﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1 : EnemyAbstract
{

    void Start()
    {
        // nowMoveTime = 0;
        hp = 1;
        slashDefense = 0;
        shotDefense = 0;
        attackPower = 5;
        score = 100;
    }

    // Update is called once per frame
    void Update()
    {
        if (isHit)
            CutedObject();

        //プレイヤーに徐々に近づく
        Vector3 sa = GameSystem.I.GetPlayerPosition() - this.transform.localPosition;
        float kyori = Mathf.Sqrt(Mathf.Pow(sa.x, 2) + Mathf.Pow(sa.y, 2));
        float moveSpeed2 = 0.05f;

        this.transform.localPosition += new Vector3(sa.x * (moveSpeed2 / kyori), sa.y * (moveSpeed2 / kyori), 0);
    }

    private new void  DestroyMe()
    {
        Destroy(this.gameObject);
    }

}
