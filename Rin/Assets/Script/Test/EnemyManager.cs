﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{

    public static EnemyManager I;

    [SerializeField]
    Enemy1 enemyPrefab;




    [SerializeField]
    UnityEngine.UI.Text enemyCountText;

    List<Enemy1> enemy = new List<Enemy1>();

    float totalTime;
    float enemyGenerationTime = 4f;//3秒ごとに生成
    int generationCount = 10;//十体でたら終了

    // Use this for initialization
    void Start()
    {
        I = this;
        totalTime = 0;
        generationCount = 10;
        //enemy.Add( Instantiate(enemyPrefab));
       // enemy.Add(Instantiate(enemyPrefab));
        //enemy.Add(Instantiate(enemyPrefab));
        //enemy[enemy.Count - 1].GetComponent<Transform>().localPosition = enemy[enemy.Count - 2].GetComponent<Transform>().localPosition + new Vector3(2,0,0);
        //enemy.Add(Instantiate(enemyPrefab));
        //enemy[enemy.Count - 1].GetComponent<Transform>().localPosition = enemy[enemy.Count - 2].GetComponent<Transform>().localPosition + new Vector3(2, 0, 0);
        //enemy.Add(Instantiate(enemyPrefab));
        //enemy[enemy.Count - 1].GetComponent<Transform>().localPosition = enemy[enemy.Count - 2].GetComponent<Transform>().localPosition + new Vector3(2, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        //いったん非難
        //totalTime += Time.deltaTime;
        //if (totalTime > enemyGenerationTime && generationCount > 0)
        //{
        //    totalTime = 0f;
        //    generationCount--;
        //    enemy.Add(Instantiate(enemyPrefab));

        //    enemy[enemy.Count - 1].transform.position = GameSystem.I.GetPlayerPosition() + new Vector3(10, Random.Range(-2.0f, 6.0f), 0);

        //}

    }
    
    //死んだエネミーをリストから削除
    public void CheckEnemyList(Enemy1 deleteEnemy)
    {
        for(int i=0;i<enemy.Count;i++)
        {
            if(deleteEnemy==enemy[i])
            {
                enemy.Remove(enemy[i]);
                if (generationCount==0&&enemy.Count == 0)//全て死んだら終了
                    GameSystem.I.GameOver();

            }
        }
    }

    public void GenerateEnemy()
    {
        Instantiate(enemyPrefab);
    }
   

    ////タップ時のエネミーとの当たり判定
    //public void CheckEnemyTop(Vector2 touchPosition)
    //{
    //    float topArea = 15;//タップ時の当たり判定
    //    for(int i=0;i<enemy.Count;i++)
    //    {
    //       // enemy[i].GetComponent<CircleCollider2D>().
    //    }
    //}


}
