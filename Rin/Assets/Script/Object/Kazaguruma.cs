﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kazaguruma : MonoBehaviour
{
    const int Point = 100;//適当

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }


    void OnTriggerEnter2D(Collider2D c)
    {

        if (c.tag == "ShotAttack"|| c.tag == "SlashAttack")
        {
            GameSystem.I.AddScore(Point);
            GameObject go = Instantiate(Resources.Load("KazagurumaParticle")) as GameObject;
            go.transform.position = this.transform.position;

        }
    }

}
