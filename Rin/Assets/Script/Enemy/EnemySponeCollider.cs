﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySponeCollider : MonoBehaviour
{
    [SerializeField]
    GameObject MyActiveObject;

    void OnTriggerEnter2D(Collider2D c)
    {
        string tag = c.tag;
        if (tag == "Player")
        {
            MyActiveObject.active = true;
            Destroy(this.gameObject.GetComponent<BoxCollider2D>());//コリダーのみ消す
        }
    }
}
