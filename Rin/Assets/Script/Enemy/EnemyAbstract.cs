﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyAbstract : MonoBehaviour
{

    //各ステータス
    protected int hp;
    protected int slashDefense;
    protected int shotDefense;
    protected int attackPower;
    protected int score;

    //斬撃用（カット後に）
    protected List<GameObject> cutedObject = new List<GameObject>();
    protected Vector3 SavePosition;
    protected Vector3[] syosokudo;
    protected float[] kakudoRadian;
    protected float totalTime;
    protected bool isHit;

    // Update is called once per frame
    protected void Update()
    {

        if (isHit)
            CutedObject();

    }


    protected void CutedObject()
    {
        //カット後に位置がずれてから
        if (SavePosition != cutedObject[0].transform.position)
        {

            Debug.Log("");
            //初回一回目に初速度、角度求める
            if (syosokudo[0] == Vector3.zero)
            {
                Debug.Log("");
                float moveSpeed = 4.5f;
                for (int i = 0; i < cutedObject.Count; i++)
                {
                    Vector3 selectPosition = cutedObject[i].transform.position;
                    //初速度求める
                    Vector3 add = selectPosition - SavePosition;
                    syosokudo[i] = add.normalized * moveSpeed;
                    //角度
                    kakudoRadian[i] = Mathf.Atan2(
                        selectPosition.y - SavePosition.y,
                        selectPosition.x - SavePosition.x);
                }
            }
            Debug.Log("");
            //斜方投射、今回ー前回で移動文加算
            float zyuryoku = 1.5f;
            for (int i = 0; i < cutedObject.Count; i++)
            {
                cutedObject[i].transform.position += new Vector3(
                    syosokudo[i].x * Mathf.Cos(kakudoRadian[i]) * (totalTime + Time.deltaTime)
                    - syosokudo[i].x * Mathf.Cos(kakudoRadian[i]) * totalTime,
            syosokudo[i].y * Mathf.Sin(kakudoRadian[i]) * (totalTime + Time.deltaTime) - 0.5f * zyuryoku * Mathf.Pow(totalTime + Time.deltaTime, 2)
            - syosokudo[i].y * Mathf.Sin(kakudoRadian[i]) * totalTime - 0.5f * zyuryoku * Mathf.Pow(totalTime, 2)
                    , 0);
            }
            totalTime += Time.deltaTime;
            //5秒たったら削除（適当、変更？）
            if (totalTime > 5f)
            {
                for (int i = 0; i < cutedObject.Count; i++)
                {
                    Destroy(cutedObject[i]);
                }
                Destroy(this.gameObject.transform.parent.gameObject);
            }
        }
    }

    protected void OnTriggerEnter2D(Collider2D c)
    {
        // 弾の削除
        if (c.tag == "SlashAttack")
        {
            hp -= GameSystem.SlashPower - slashDefense;
            if (hp > 0)
            {
                //赤く点滅演出
                //同じ線に切られないような工夫ー＞ランダムでidでもつける？
                return;
            }
            GameSystem.I.AddScore(score);
            isHit = true;
            //今スラッシュしたオブジェクトの角度に切り裂く
            float ratation = (c.transform.localEulerAngles.z) * Mathf.PI / 180;
            // Debug.Log("ratatioaan22=" + c.transform.localEulerAngles.z);

            Vector3 nowPosition = c.transform.position;
            //   Debug.Log("enemyPosition" + nowPosition);
            Vector3 stratPosition = new Vector3(2, 2, 0) + nowPosition;
            Vector3 endPosition = new Vector3(-2, -2, 0) + nowPosition;

            stratPosition = new Vector3(
                Mathf.Cos(ratation) * (stratPosition.x - nowPosition.x) - Mathf.Sin(ratation) * (stratPosition.y - nowPosition.y) + nowPosition.x,
                Mathf.Sin(ratation) * (stratPosition.x - nowPosition.x) + Mathf.Cos(ratation) * (stratPosition.y - nowPosition.y) + nowPosition.y,
                0
                );
            endPosition = new Vector3(
            Mathf.Cos(ratation) * (endPosition.x - nowPosition.x) - Mathf.Sin(ratation) * (endPosition.y - nowPosition.y) + nowPosition.x,
            Mathf.Sin(ratation) * (endPosition.x - nowPosition.x) + Mathf.Cos(ratation) * (endPosition.y - nowPosition.y) + nowPosition.y,
            0
            );
            List<SpriteSlicer2DSliceInfo> cutObject = new List<SpriteSlicer2DSliceInfo>();

            SpriteSlicer2D.SliceSprite(stratPosition, endPosition, this.gameObject, false, ref cutObject);

            for (int i = 0; i < cutObject.Count; i++)
            {
                for (int j = 0; j < cutObject[i].ChildObjects.Count; j++)
                {
                    Destroy(cutObject[i].ChildObjects[j].GetComponent<PolygonCollider2D>());
                }
            }

            for (int i = 0; i < cutObject[0].ChildObjects.Count; i++)
                cutedObject.Add(cutObject[0].ChildObjects[i]);
            SavePosition = cutObject[0].SlicedObject.transform.position;
            syosokudo = new Vector3[cutedObject.Count];
            syosokudo[0] = Vector3.zero;
            kakudoRadian = new float[cutedObject.Count];
            totalTime = 0;
            Destroy(gameObject.GetComponent<SpriteRenderer>());
            gameObject.SetActive(true);
            this.enabled = true;
            Destroy(gameObject.GetComponent<PolygonCollider2D>());
        }
        else if (c.tag == "Player")
        {
            //     Debug.Log("プレイヤーと当たりました");
            //死ぬ演出あるかも？
            GameSystem.I.GetPlayer().Damage(attackPower);
            DestroyMe();
        }
        else if (c.tag == "ShotAttack")
        {
            hp -= GameSystem.ShotPower - shotDefense;
            if (hp > 0)
            {
                //赤く点滅演出
                return;
            }
            GameSystem.I.AddScore(score);
            Destroy(this.gameObject.transform.parent.gameObject);

        }
    }

    protected void DestroyMe()
    {
        Destroy(this.gameObject.transform.parent.gameObject);
    }
}
