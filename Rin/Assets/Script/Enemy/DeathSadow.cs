﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DeathSadow : EnemyAbstract
{
    float moveSpeed = 0.06f;
    [SerializeField]
    SpriteRenderer shadowSpriteRender;
    Vector3 addScale;
    void Start()
    {
        hp = 30;
        slashDefense = 0;
        shotDefense = 25;
        attackPower = 30;
        score = 1000;


        addScale = transform.localScale * 3.8f;
        transform.DOScale(addScale, 1.5f);
        transform.DOMoveY(-1f, 1.5f);
    }
    new void Update()
    {
        if (isHit)
            CutedObject();
        if (transform.localScale == addScale)
        {
            if (shadowSpriteRender != null)
                DeleteShadow();

            if (transform.position.x <GameSystem.I.GetPlayerPosition().x)
            {
                transform.position += new Vector3(moveSpeed, 0);
            }
            else
            {
                transform.position += new Vector3(-moveSpeed, 0);
            }
        }
    }
    private void DeleteShadow()
    {
        Destroy(shadowSpriteRender);
    }

    //TODO:上手く言ってない直す
    private new void  DestroyMe()//親の親
    {
        Destroy(gameObject.transform.parent.gameObject.transform.parent.gameObject);
    }

}
