﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Karasu : EnemyAbstract
{
    private float UpdateTime;
    private float DotweenTime = 0;

    [SerializeField]
    List<Vector3> targetPosition = new List<Vector3>();

    [SerializeField]
    List<float> moveTime = new List<float>();

    void Start()
    {
        hp = 30;
        slashDefense = 0;
        shotDefense = 0;
        attackPower = 10;
        score = 300;

        UpdateTime = 0;
        foreach (float t in moveTime)
        {
            DotweenTime += t;
        }

        StartCoroutine("MoveCoRutine");
    }
    private IEnumerator MoveCoRutine()
    {

        for (int listCount = 0; listCount < targetPosition.Count; listCount++)
        {
            transform.DOMove(targetPosition[listCount], moveTime[listCount]);
            if (listCount < targetPosition.Count && listCount > 0)
            {
                if (targetPosition[listCount].x < targetPosition[listCount - 1].x)
                {
                    transform.eulerAngles = new Vector3(0, 180, 0);
                }
                else
                {
                    transform.eulerAngles = new Vector3(0, 0, 0);
                }
            }
            yield return new WaitForSeconds(moveTime[listCount]);
        }
    }

    void Update()
    {

        if (isHit)
            CutedObject();

        UpdateTime += Time.deltaTime;
        if (UpdateTime > DotweenTime)
        {
            Destroy(transform.parent.gameObject);
        }
    }
}
